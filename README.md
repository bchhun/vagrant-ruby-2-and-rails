# vagrant-ruby-2-and-rails

Made to scratch my own itch.

I want to install a bleeding edge Ruby dev environment using vagrant.

I'm using vagrant's fabric plugin to:

* install `ruby 2.1.0` using `rbenv` and set that version as the global one
* setup any kind of database as well

## Dependencies

### Python

    $ sudo apt-get install python-setuptools python-dev python-pip

**note:** this would only work in a debian-flavored linux box. Replace `apt-get`
with whatever your system is using as a package manager.

### Fabric

    $ sudo pip install fabric

## Starting the VM

	$ fab up # a vagrant up alias but with provisionning managed by fabric

### Special notice for Windows user

If you're a windows user, you've probably ran into problems trying to compile PyCrypto.

#### 1. Download the following [precompiled PyCrypto package](http://www.voidspace.org.uk/downloads/pycrypto-2.1.0.win32-py2.7.zip)

#### 2. Unzip it

#### 3. Get into the folder

#### 4. Run the following command to install it on your system:

	$ python setup.py install

#### 5. You'll then need to install paramiko (a fabric dependency) by hand:

	$ pip install paramiko

You're now good to use:

	$ fab up

