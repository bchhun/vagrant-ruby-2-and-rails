#coding: utf-8

import os.path as op
from fabric.api import local, task, sudo, run, cd, env

RUBY_VERSION = "2.1.0"
RBENV_CMD = "~/.rbenv/bin/rbenv"

VM_IP = "10.0.0.17"

env.roledefs['vagrant'] = ["vagrant@%s" % VM_IP]
env.roles = ['vagrant']
 
role = env.roles[0]
 
if role == 'vagrant':
    if not op.exists(op.expanduser('~/.ssh/config')):
        print "The ~/.ssh/config doesn't exist. It needs to be there for the next command to work. lambda config creation."
        
        with open(op.expanduser('~/.ssh/config'), 'wt') as fp:
            fp.write("IdentityFile ~/.ssh/id_rsa")
    
    local('ssh-add %s' % op.expanduser('~/.vagrant.d/insecure_private_key'))

@task
def provision():
    """
        Provision the VM
    """
    
    sudo("apt-get -y update")

    modules = ["build-essential", "git-core"]
    sudo("apt-get install " + " ".join(modules))

    run("git clone https://github.com/sstephenson/rbenv.git ~/.rbenv")
    run("echo 'export PATH=\"$HOME/.rbenv/bin:$PATH\"' >> ~/.bashrc")
    run("echo 'eval \"$(rbenv init -)\"' >> ~/.bashrc")

    run("git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build")

    run("git clone https://github.com/carsomyr/rbenv-bundler.git ~/.rbenv/plugins/bundler")

    sudo("%s install %s" % (RBENV_CMD, RUBY_VERSION))
    sudo("%s global %s" % (RBENV_CMD, RUBY_VERSION))

    # [TODO] these steps below need to be done manually for now
    sudo("gem install bundler")
    sudo("%s rehash" % RBENV_CMD)

    with cd("~/my-project"):
        run("bundle install")

@task
def up():
    """
    vagrant up + provisionning
    """

    local("vagrant up")
    provision()
